# Automated Testing with RSpec and Capybara

## RSpec

[RSpec](http://rspec.info/) is testing tool for the Ruby programming language.
Born under the banner of Behaviour-Driven Development, it is designed to make
Test-Driven Development a productive and enjoyable experience with features like:

* a rich command line program (the rspec command)
* textual descriptions of examples and groups (rspec-core)
* flexible and customizable reporting
* extensible expectation language (rspec-expectations)
* built-in mocking/stubbing framework (rspec-mocks)


### Installing RSpec

To get started with RSpec in a rails application add the following to your Gemfile:

```ruby
group :development, :test do
  gem 'rspec-rails'
end
```

Once your gemfile has been updated, run `bundle install` to install the rspec gem.

Then initialize the `spec/` directory (where your tests will live) by running:
`rails g rspec:install`

This will add `spec/spec_helper.rb` and `.rspec` files that are used for
configuration.

### Model Specs (Unit Tests)

Model specs live in the `spec/models` directory.  Model specs are used to unit
test models in the application.

```ruby
require 'spec_helper'

describe User do
  it 'orders by last name' do
    peep = User.create(first_name: "Bo", last_name: "Peep")
    appleseed = User.create(first_name: "Jonny", last_name: "Appleseed")
    expect(User.ordered_by_last_name).to eq([appleseed, peep])
  end
end
```
### Controller Specs

Controller specs reside in the spec/controllers folder. Use controller specs to
describe behavior of Rails controllers.

```ruby
require "spec_helper"

describe PostsController do
  describe "GET #index" do
    it "responds successfully with an HTTP 200 status code" do
      get :index
      expect(response).to be_success
      expect(response.status).to eq(200)
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end

    it "loads all of the posts into @posts" do
      post1, post2 = Post.create!, Post.create!
      get :index

      expect(assigns(:posts)).to match_array([post1, post2])
    end
  end
end
```

### Running Your Tests

To run your rspec tests run:

```bash
bundle exec rspec
```

If you are using RSpec through `rspec-rails` then you can also run all your tests
using the included rake task:

```bash
rake rspec
```

### DRY'ing Up Your Tests

#### Extract common setup to `before` blocks
Use before and after hooks to execute arbitrary code before and/or
after the body of an example is run:

```ruby
# before(:each)  run before each example
# before(:all)   run one time only, before all of the examples in a group
# after(:each)   run after each example
# after(:all)    run one time only, after all of the examples in a group

describe Thing do
  before(:each) do
    @thing = Thing.new
  end

  describe "initialized in before(:each)" do
    it "has 0 widgets" do
      @thing.should have(0).widgets
    end

    it "can get accept new widgets" do
      @thing.widgets << Object.new
    end

    it "does not share state across examples" do
      @thing.should have(0).widgets
    end
  end
```

#### Use `let` statements  
Use let to define a memoized helper method. The value will be cached
across multiple calls in the same example but not across examples.  

Note that let is lazy-evaluated: it is not evaluated until the first time
the method it defines is invoked. You can use let! to force the method's
invocation before each example.

```ruby
$count = 0
describe "let" do
  let(:count) { $count += 1 }

  it "memoizes the value" do
    count.should == 1
    count.should == 1
  end

  it "is not cached across examples" do
    count.should == 2
  end
end
```

## Capybara

[Capybara](http://jnicklas.github.io/capybara/) helps you test web applications
by simulating how a real user would interact with your app; Allowing you to treat
the application as a black box and test only the final outputs without worrying
about the internal state of the application.

### Installing Capybara

To get started using Capybara with rspec add the following to your Gemfile:

```ruby
group :test do
  gem 'capybara'
end
```

Once your gemfile has been updated, run `bundle install` to install capybara.

To load Capybara into RSpec you must add the followign to your `spec/spec_helper.rb`:

```
require 'capybara/rspec'
```


### Writing Feature (Integration) Tests with Capybara

Now that you have Capybara installed and able to be used with RSpec, you can
start writing feature and acceptance tests.

Feature tests reside in the `spec/features` directory and are used to test
application features from the point of view of the user.

```ruby
describe "the signin process", :type => :feature do
  before :each do
    User.make(:email => 'user@example.com', :password => 'caplin')
  end

  it "signs me in" do
    visit '/sessions/new'
    within("#session") do
      fill_in 'Login', :with => 'user@example.com'
      fill_in 'Password', :with => 'password'
    end
    click_link 'Sign in'
    expect(page).to have_content 'Success'
  end
end
```
